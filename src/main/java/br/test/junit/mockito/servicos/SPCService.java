package br.test.junit.mockito.servicos;


import br.test.junit.mockito.entidades.Usuario;

public interface SPCService {

	public boolean possuiNegativacao(Usuario usuario) throws Exception;
}
