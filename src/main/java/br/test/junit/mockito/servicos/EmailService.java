package br.test.junit.mockito.servicos;


import br.test.junit.mockito.entidades.Usuario;

public interface EmailService {
	
	public void notificarAtraso(Usuario usuario);

}
