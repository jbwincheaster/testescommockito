package br.test.junit.mockito.daos;

import br.test.junit.mockito.entidades.Locacao;

import java.util.List;


public interface LocacaoDAO {

	public void salvar(Locacao locacao);

	public List<Locacao> obterLocacoesPendentes();
}
