package br.test.junit.mockito.suites;

import br.test.junit.mockito.servicos.CalculadoraTest;
import br.test.junit.mockito.servicos.CalculoValorLocacaoTest;
import br.test.junit.mockito.servicos.LocacaoServiceTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	//CalculadoraTest.class,
	CalculoValorLocacaoTest.class,
	LocacaoServiceTest.class
})
public class SuiteExecucao {
	//Remova se puder!
}
