package br.test.junit.mockito.matchers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.test.junit.mockito.utils.DataUtils;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class DataDiferencaDiasMatcher extends TypeSafeMatcher<Date> {

	private Integer quantidadeDeDias;
	
	public DataDiferencaDiasMatcher(Integer qtdDias) {
		this.quantidadeDeDias = qtdDias;
	}
	
	public void describeTo(Description desc) {
		Date dataEsperada = DataUtils.obterDataComDiferencaDias(quantidadeDeDias);
		DateFormat format = new SimpleDateFormat("dd/MM/YYYY");
		desc.appendText(format.format(dataEsperada));
	}

	@Override
	protected boolean matchesSafely(Date data) {
		return DataUtils.isMesmaData(data, DataUtils.obterDataComDiferencaDias(quantidadeDeDias));
	}

}
